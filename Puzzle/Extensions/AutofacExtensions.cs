﻿using System;
using System.Reflection;
using Autofac;
using Autofac.Builder;
using Autofac.Core;

namespace Puzzle.Extensions
{
    public static class AutofacExtensions
    {
        /// <remarks>
        /// Copied from <see cref="T:Autofac.Util.ReflectionExtensions">Autofac.Util.ReflectionExtensions</see>.
        /// </remarks>
        public static bool TryGetDeclaringProperty(this ParameterInfo pi, out PropertyInfo prop)
        {
            MethodInfo methodInfo = pi.Member as MethodInfo;
            if (methodInfo != null && methodInfo.IsSpecialName && (methodInfo.Name.StartsWith("set_", StringComparison.Ordinal) && methodInfo.DeclaringType != null))
            {
                prop = methodInfo.DeclaringType.GetProperty(methodInfo.Name.Substring(4));
                return true;
            }
            else
            {
                prop = (PropertyInfo)null;
                return false;
            }
        }

        /// <remarks>
        /// Copied from http://stackoverflow.com/questions/9710779/autofac-resolve-a-specific-dependency-to-a-named-instance
        /// </remarks>
        public static IRegistrationBuilder<TLimit, TReflectionActivatorData, TStyle> WithResolvedProperty<TLimit, TReflectionActivatorData, TStyle, TProperty>(
            this IRegistrationBuilder<TLimit, TReflectionActivatorData, TStyle> registration,
            string propertyName,
            Func<IComponentContext, TProperty> valueProvider)
            where TReflectionActivatorData : ReflectionActivatorData
        {
            return registration.WithProperty(
                new ResolvedParameter(
                    (p, c) =>
                    {
                        PropertyInfo prop;
                        return p.TryGetDeclaringProperty(out prop) &&
                               prop.Name == propertyName;
                    },
                    (p, c) => valueProvider(c)));
        }
    }
}