﻿using Puzzle.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Puzzle.Business;

namespace Puzzle.Controllers
{
    public class ShapeSketcherController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            ShapeSketcherRequest request = new ShapeSketcherRequest();

            // populate menus before showing
            PuzzleThree puzzle = new PuzzleThree();
            request.MenuItemList = puzzle.GetMenuItems();

            return View(request);
        }

        [HttpGet]
        public ActionResult Select(int shapeType)
        {
            // Check the Shape and display the correct fields
            ShapeSketcherRequest request = new ShapeSketcherRequest();
            request.ShapeType = shapeType;
            return View(request);
        }
    }
}