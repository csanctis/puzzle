﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Puzzle.Business;
using Puzzle.Models;

namespace Puzzle.Controllers
{
    public class PalindromeController : Controller
    {
        IPuzzleTwo PuzzleTwo { get; set; }

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(PalindromeRequest request)
        {
            if (!ModelState.IsValid)
            {
                return View(request);
            }

            request.isPalindrome = false;

            PuzzleTwo = new PuzzleTwo();
            request.isPalindrome = PuzzleTwo.IsPalindrome(request.Sentence);

            return View(request);
        }
    }
}