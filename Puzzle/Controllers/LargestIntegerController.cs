﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Puzzle.Business;
using Puzzle.Models;

namespace Puzzle.Controllers
{
    public class LargestIntegerController : Controller
    {
        public IPuzzleOne PuzzleOne { get; set; }

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(PuzzleOneRequest request)
        {
            if (!ModelState.IsValid)
            {
                return View(request);
            }

            request.LargestNumber = -1;

            PuzzleOne = new PuzzleOne();
            request.LargestNumber = PuzzleOne.findLargestInt(request.Sequence);

            return View(request);
        }
    }
}
