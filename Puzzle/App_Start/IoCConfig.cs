﻿using System;
using System.Configuration;
using System.Reflection;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using log4net;
using Puzzle.Business;

namespace Puzzle
{
    public class IoCConfig
    {
        public static void Register()
        {
            var builder = new ContainerBuilder();
            // Logger
            builder.RegisterInstance(LogManager.GetLogger("Logger")).As<ILog>()
                .SingleInstance();

            // Register your MVC controllers.
            builder.RegisterControllers(typeof(MvcApplication).Assembly);

            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly())
                .Where(t => t.Name.EndsWith("Validator"))
                .AsImplementedInterfaces();

            builder.RegisterModelBinders(Assembly.GetExecutingAssembly());
            builder.RegisterModelBinderProvider();

            // Custom classes
            builder.RegisterType<PuzzleOne>().As<IPuzzleOne>()
                .PropertiesAutowired();

            // OPTIONAL: Register web abstractions like HttpContextBase.
            builder.RegisterModule<AutofacWebTypesModule>();

            // OPTIONAL: Enable property injection into action filters.
            builder.RegisterFilterProvider();

            // Set the dependency resolver to be Autofac.
            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}