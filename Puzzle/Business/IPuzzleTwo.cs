﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Puzzle.Business
{
    public interface IPuzzleTwo
    {
        bool IsPalindrome(string str);
    }
}
