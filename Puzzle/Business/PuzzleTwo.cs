﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;

namespace Puzzle.Business
{
    public class PuzzleTwo : IPuzzleTwo
    {
        public bool IsPalindrome(string sentence)
        {
            // Replace invalid characters with empty strings
            Regex regex = new Regex("[^a-zA-Z]");
            var text = regex.Replace(sentence, "").ToLower(); 
            
            int min = 0;
            int max = text.Length - 1;
            while (true)
            {
                if (min > max)
                {
                    return true;
                }
                char a = text[min];
                char b = text[max];
                if (char.ToLower(a) != char.ToLower(b))
                {
                    return false;
                }
                min++;
                max--;
            }
        }
    }
}