﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Puzzle.Models;

namespace Puzzle.Business
{
    public class PuzzleThree
    {
        public List<MenuItem> GetMenuItems()
        {
            List<MenuItem> MenuItemList = new List<MenuItem>();

            /* Add the Triangle Menu */
            MenuItem triangleMenu = new MenuItem
            {
                Value = 0,
                Text = "Triangles"
            };
            triangleMenu.MenuItemList = new List<MenuItem>();

            foreach (TriangleType type in Enum.GetValues(typeof(TriangleType)))
            {
                int x = (int)type;
                MenuItem menuItem = new MenuItem
                {
                    Value = x,
                    Text = type.ToString()
                };
                triangleMenu.MenuItemList.Add(menuItem);
            }

            MenuItemList.Add(triangleMenu);

            /* Add the Quadrilaterals Menu */
            MenuItem quadrilateralsMenu = new MenuItem
            {
                Value = 0,
                Text = "Quadrilaterals"
            };
            quadrilateralsMenu.MenuItemList = new List<MenuItem>();

            foreach (QuadrilateralsType type in Enum.GetValues(typeof(QuadrilateralsType)))
            {
                int x = (int)type;
                MenuItem menuItem = new MenuItem
                {
                    Value = x,
                    Text = type.ToString()
                };
                quadrilateralsMenu.MenuItemList.Add(menuItem);
            }

            MenuItemList.Add(quadrilateralsMenu);

            /* Add the Polygons Menu */
            MenuItem polygonsMenu = new MenuItem
            {
                Value = 0,
                Text = "Polygons"
            };
            polygonsMenu.MenuItemList = new List<MenuItem>();

            foreach (PolygonsType type in Enum.GetValues(typeof(PolygonsType)))
            {
                int x = (int)type;
                MenuItem menuItem = new MenuItem
                {
                    Value = x,
                    Text = type.ToString()
                };
                polygonsMenu.MenuItemList.Add(menuItem);
            }

            MenuItemList.Add(polygonsMenu);

            /* Add the Curved Shapes Menu */
            MenuItem curvedMenu = new MenuItem
            {
                Value = 0,
                Text = "Curved Shapes"
            };
            curvedMenu.MenuItemList = new List<MenuItem>();

            foreach (Curvedype type in Enum.GetValues(typeof(Curvedype)))
            {
                int x = (int)type;
                MenuItem menuItem = new MenuItem
                {
                    Value = x,
                    Text = type.ToString()
                };
                curvedMenu.MenuItemList.Add(menuItem);
            }

            MenuItemList.Add(curvedMenu);
            
            return MenuItemList;
        }
    }

    public class MenuItem
    {
        public int Value { get; set; }
        public string Text { get; set; }
        public List<MenuItem> MenuItemList { get; set; }
    }
}