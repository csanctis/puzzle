﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Puzzle.Business
{
    public class PuzzleOne : IPuzzleOne
    {
        public int findLargestInt(string csv)
        {
            if (string.IsNullOrEmpty(csv))
            {
                // Invalid sequence
                return -1;
            }

            int largestNumber = 0;
            string[] possibleNumbers = csv.Split(',');
            foreach (string possibleNumber in possibleNumbers)
            {
                try
                {
                    int number = Convert.ToInt32(possibleNumber.Replace(" ", ""));
                    if (number > 0 && number > largestNumber)
                    {
                        largestNumber = number;
                    }
                }
                catch (Exception)
                {
                    // Invalid number. Ignore it.
                }
            }

            return largestNumber;
        }
    }
}