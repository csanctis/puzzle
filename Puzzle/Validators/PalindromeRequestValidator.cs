﻿using FluentValidation;
using Puzzle.Models;

namespace Puzzle.Validators
{
    public class PalindromeRequestValidator : AbstractValidator<PalindromeRequest>
    {
        public PalindromeRequestValidator()
        {
            RuleFor(u => u.Sentence).NotEmpty().Length(1, 100);
        }
    }
}