﻿using FluentValidation;
using Puzzle.Models;

namespace Puzzle.Validators
{
    public class PuzzleOneRequestValidator : AbstractValidator<PuzzleOneRequest>
    {
        public PuzzleOneRequestValidator()
        {
            RuleFor(u => u.Sequence).NotEmpty().Length(1, 100);            
        }
    }
}