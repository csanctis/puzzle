﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using FluentValidation;
using Puzzle.Extensions;
using Puzzle.Business;

namespace Puzzle.Models
{
    public class ShapeSketcherRequest
    {
        [Display(Name = "Shape")]
        public int ShapeType { get; set; }
        public List<MenuItem> MenuItemList { get; set; }
    }
}