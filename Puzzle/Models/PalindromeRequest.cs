﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using FluentValidation;
using System.Web;
using System.Web.Mvc;
using Puzzle.Extensions;

namespace Puzzle.Models
{
    public class PalindromeRequest : IValidatableObject
    {
        [Display(Name = "Sentence")]
        public string Sentence { get; set; }

        [Display(Name = "Is Palindrome?")]
        public bool isPalindrome { get; set; }

        public IEnumerable<ValidationResult> Validate(System.ComponentModel.DataAnnotations.ValidationContext validationContext)
        {
            return DependencyResolver.Current.GetService<IValidator<PalindromeRequest>>().Validate(this).ToValidationResult();
        }
    }
}