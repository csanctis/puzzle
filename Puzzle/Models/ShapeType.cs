﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Puzzle.Models
{
    public enum TriangleType
    {
        Isosceles = 1,
        Scalene = 2,
        Equilateral = 3,
        Right = 4,
        Obtuse = 5,
        Acute = 6
    }

    public enum QuadrilateralsType
    {
        Rectangle = 7,
        Square = 8,
        Parallelogram = 9
    }

    public enum PolygonsType
    {
        Pentagon = 10,
        Hexagon = 11,
        Heptagon = 12,
        Octagon = 13
    }

    public enum Curvedype
    {
        Circle = 14,
        Oval = 15
    }
}
