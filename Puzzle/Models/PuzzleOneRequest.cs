﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using FluentValidation;
using System.Web;
using System.Web.Mvc;
using Puzzle.Extensions;


namespace Puzzle.Models
{
    public class PuzzleOneRequest: IValidatableObject
    {
        [Display(Name = "Sequence of Numbers")]
        public string Sequence { get; set; }
        [Display(Name = "Largest Number")]
        public int LargestNumber { get; set; }

        public IEnumerable<ValidationResult> Validate(System.ComponentModel.DataAnnotations.ValidationContext validationContext)
        {
            return DependencyResolver.Current.GetService<IValidator<PuzzleOneRequest>>().Validate(this).ToValidationResult();
        }
    }
}